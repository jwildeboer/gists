How to do a Jekyll build and upload to codeberg pages the robust way:

```
# .woodpecker.yml
steps:
  - name: BuildAndPush
    when:
    - event: manual        # workflow should run if you execute it manual
    - event: push          # workflow should run on each push ( = on each commit)
    image: jekyll/jekyll
    secrets: [ cbtoken, cbmail ]
    commands:
      - chmod -R 777 .
      - bundle install
      - bundle exec jekyll build
      - git config --global --add safe.directory /woodpecker/src/codeberg.org/jwildeboer/fedigridsource/_site
      - cp domains _site/.domains
      - cd _site
      - git init
      - git branch -m pages
      - git config user.email "$CBMAIL"
      - git config user.name "CI Builder"
      - git remote add origin https://$CBTOKEN@codeberg.org/jwildeboer/fedigrid.git
      - git add --all
      - git commit -m "Woodpecker CI Jekyll Build"
      - git push -u origin pages -f
```

And here in a slightly more respectful way (that doesn't destroy the git history every time ;):

```
# .woodpecker.yml
steps:
  - name: BuildAndPush
    when:
    - event: manual        # workflow should run if you execute it manual
    - event: push          # workflow should run on each push ( = on each commit)
    image: jekyll/jekyll
    secrets: [ cbtoken, cbmail ]
    commands:
      - chmod -R 777 .
      - git config --global --add safe.directory /woodpecker/src/codeberg.org/jwildeboer/jwildeboersource/_site
      - git config --global user.email "$CBMAIL"
      - git config --global user.name "CI Builder"
      - git config --global init.defaultBranch pages
      - git clone https://codeberg.org/jwildeboer/jwildeboer.git
      - mv jwildeboer _site
      - cd _site
      - git branch -m pages
      - git remote set-url origin https://$CBTOKEN@codeberg.org/jwildeboer/jwildeboer.git
      - chmod -R 777 .
      - cd ..
      - bundle install
      - bundle exec jekyll build
      - cp domains _site/.domains
      - cd _site
      - git add --all
      - git commit -m "Woodpecker CI Jekyll Build"
      - git push
```
