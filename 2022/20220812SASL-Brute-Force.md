Find IP addresses with

`grep "SASL LOGIN authentication failed" /var/log/maillog | awk '{ print $7 }' | sort | uniq -c`

or

`grep "NOQUEUE:" /var/log/maillog | awk '{ print $10 }' | sort | uniq -c`

get list:

```
     1 unknown[117.4.136.219]:
      1 unknown[118.69.186.39]:
      1 unknown[119.64.179.241]:
     90 unknown[141.98.10.108]:
     98 unknown[141.98.10.203]:
     97 unknown[141.98.10.217]:
     98 unknown[141.98.10.24]:
     94 unknown[141.98.10.27]:
     95 unknown[141.98.10.70]:
     88 unknown[141.98.10.81]:
    105 unknown[141.98.10.82]:
     86 unknown[141.98.10.84]:
     89 unknown[141.98.11.112]:
     92 unknown[141.98.11.113]:
     92 unknown[141.98.11.119]:
     96 unknown[141.98.11.19]:
     96 unknown[141.98.11.37]:
     95 unknown[141.98.11.51]:
     93 unknown[141.98.11.74]:
     94 unknown[141.98.11.75]:
     90 unknown[141.98.11.81]:
     95 unknown[141.98.11.95]:
      1 unknown[162.142.99.165]:
      1 unknown[166.215.55.208]:
      1 unknown[168.90.157.26]:
      1 unknown[188.255.63.9]:
      1 unknown[188.92.214.111]:
      1 unknown[2.37.223.58]:
      1 unknown[201.160.58.203]:
      1 unknown[220.194.148.251]:
      1 unknown[222.252.20.109]:
      1 unknown[37.139.109.51]:
      1 unknown[43.251.255.38]:
      1 unknown[43.251.255.92]:
     96 unknown[45.125.65.159]:
     93 unknown[45.125.65.37]:
     92 unknown[45.125.66.22]:
     97 unknown[45.125.66.24]:
    100 unknown[45.125.66.55]:
      1 unknown[75.132.124.46]:
     94 unknown[91.224.92.110]:
```

Use whois and send to abuse address 