# Updating masto-thread-renderer

[masto-thread-renderer](https://github.com/vrutkovs/masto-thread-renderer) is a little #OpenSource (Apache2 license) project by [@vadim@social.vrutkovs.eu](https://social.vrutkovs.eu/@vadim) that you feed with a link to the start of a thread on #Mastodon and it will fetch the complete thread (however, only the toots from the original author of the thread, no replies from other people) and render it as #Markdown which you can copy/paste to use as blogpost or gist or whatever else you want to do with it.

We use the Dockerfile and `podman build` to create a new container with the latest version, create a systemd service file und restart the service. I run my instance of masto-thread-renderer at [mtr.wildeboer.net](https://mtr.wildeboer.net) behind nginx, which takes care of the proxying and letsencrypt certificates.

## Get the newest version

Have the repo cloned locally

```
> git clone https://github.com/vrutkovs/masto-thread-renderer.git
````

Or if already there, a simple `git pull`

## Create a new container

As `masto-thread-renderer` is ephemeral and self-contained, we can simply delete the existing container (I do that with cockpit) and systemd service and create a new container.

```
> systemctl stop mtr
> systemctl disable mtr
> rm /etc/systemd/system/mtr.service
```
We build the new image from the Dockerfile in the git repo.

```
> podman build -t mtr .
```

After the image is made, I again go to cockpit and create a new container based on `localhost/mtr:latest`. Just make sure that I forward the host port to 8080 inside the container ;) I start it from cockpit and check if it works as expected.

## Create a new service file

With the new container still running, I create a new service file.

```
> podman generate systemd --restart-policy=always -t 1 mtr > /etc/systemd/system/mtr.service
```

I then edit this new service file if needed. If all is good, I stop the container in cockpit and enable and start the container as service.

```
> systemctl daemon-reload
> systemctl enable mtr
> systemctl start mtr
```

A final check and done.