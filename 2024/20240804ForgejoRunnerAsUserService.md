# Running the forgejo-runner as user systemd service on RHEL9

**Disclaimer**: This is mostly a braindump of what (I think) I did in the correct order. YMMV.

**Goal**: run the forgejo-runner as user `actions` so that it can run workflows (CI/CD) in a slightly safer way.

This gist was inspired a lot by [François Kooman's Howto for Fedora](https://code.forgejo.org/forgejo/runner/src/branch/main/scripts/systemd.md). However, his approach somehow didn't work for me on RHEL 9. So after a lot of trial and error, I came up with this solution, that kinda works good enough for me.

Thanks to the [forgejo copr repo by ne0l](https://copr.fedorainfracloud.org/coprs/ne0l/forgejo/) you can install and update the `forgejo-runner`  simply with

```
# dnf copr enable ne0l/forgejo 
# dnf install forgejo-runner
```

Now for the rest of the steps.

- create a runner token in your forgejo instance under Site Administration -> Actions -> Runners -> Create new runner. Copy the token, you'll need it when you register the runner.
- copy `/etc/forgejo-runner/config.dist.yml` to `/etc/forgejo-runner/config.yml` and adapt as needed (I enabled the cache and added the container images I need on this runner)
- create a user called actions `# useradd actions`
- (Add the new user to the wheel group (not sure if this is still needed, it gives sudo rights) `usermod -aG wheel actions`)
- allow user to start services without login `# loginctl enable-linger actions`
- become the user with `# su - actions`
- as user register the runner in your forgejo instance, so you get the config working. You will need the FQDN of your Forgejo instance and the toke we generated at the beginning.

```
$ forgejo-runner register --no-interactive --instance "https://YOUR.FORGEJO.DOMAIN" --name runner --token <TOKEN_HERE>  --config /etc/forgejo-runner/config.yml
``` 

- as user `systemctl --user enable podman.socket` and `systemctl --user start podman.socket` so the user can run containers
- create systemd unit file `/etc/systemd/user/forgejo-runner.service`

(the %h is a systemd variable that gets expanded to the user's home directory, %U gets replaced with the user's UID, see [systemd specifiers](https://man.archlinux.org/man/systemd.unit.5#SPECIFIERS))

```systemd
[Unit]
Description=Forgejo Runner

[Service]
Type=simple
WorkingDirectory=%h
Environment=XDG_RUNTIME_DIR=/run/user/%U
Environment=DOCKER_HOST=unix:///run/user/%U/podman/podman.sock
ExecStartPre=/usr/bin/sleep 20
ExecStart=/usr/bin/forgejo-runner --config /etc/forgejo-runner/config.yml daemon
RestartSec=5
Restart=on-failure

[Install]
WantedBy=default.target
```

- As user `systemctl --user daemon-reload`
- As user `systemctl --user enable forgejo-runner` and `systemctl --user start forgejo-runner`

Use `$ journalctl --user-unit forgejo-runner.service` to check the journal and see if all works as planned.

DONE