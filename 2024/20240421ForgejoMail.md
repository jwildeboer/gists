# How to configure Forgejo to send and receive e-mail

In the [Configuration Cheat Sheet](https://forgejo.org/docs/latest/admin/config-cheat-sheet/#mailer-mailer) you can find more details. The descriptions there are a bit terse and not really accessible, IMHO, so after a lot of trial and error I finally found out how to get it to work for me.

Most important: you must enable `ENABLE_NOTIFY_MAIL = true` under `[service]` in app.ini to activate the mail subsystem.

In `app.ini` we need to configure two sections. First for sending mail:

We are using TLS and authentication with user/password. Note that the password field is called `PASSWD` in this section.

```
[mailer]
ENABLED = true
PROTOCOL = smtp+starttls 
SMTP_ADDR = mailhub.example.com
USER = forge@example.com
PASSWD = Ap4ssw0rd
FROM = forge@example.com
ENVELOPE_FROM = forge@example.com
```

And secondly for receiving mails:

Forgejo will add a token to mails, using the `+`addressing scheme, that my mailserver happily supports. As we use TLS, we must use port 993, not 143 for imap. By setting `MAXIMUM_MESSAGE_SIZE = 0` we allow for any size of mail.

```
[email.incoming]
ENABLED = true
REPLY_TO_ADDRESS = forge+%{token}@example.com
HOST = mailhub.example.com
PORT = 993 
USERNAME = forge@example.com
PASSWORD = Ap4ssw0rd
USE_TLS = true
MAILBOX = InBox
DELETE_HANDLED_MESSAGE = true 
MAXIMUM_MESSAGE_SIZE = 0
```
