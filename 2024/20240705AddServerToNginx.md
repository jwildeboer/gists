# Add server entry, certificate etc to nginx

Assumes RHEL 9 machine with nginx, certbot up and running.

## Create directory for static pages

Create directory and make sure SELinux contexts are set correctly

```bash
> mkdir -p /var/www/DOMAIN.NAME
> restorecon -Rv /var/www/DOMAIN.NAME/
```

Can add at least an `index.html` to make sure we will see something when checking later.

## Nginx

In `/etc/nginx/nginx.conf` add a new server block:

```nginx
server {
    server_name DOMAIN.NAME;
    root /var/www/DOMAIN.NAME;
    listen 80;
    listen [::]:80;
}
```

Check with `nginx -t` that config file is correct. Restart nginx with `systemctl restart nginx`.

## certbot

Now run `certbot -d DOMAIN.NAME` to get the certificate and have it installed correctly.

```
> certbot -d DOMAIN.NAME
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Requesting a certificate for DOMAIN.NAME

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/DOMAIN.NAME/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/DOMAIN.NAME/privkey.pem
This certificate expires on 2024-10-03.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

Deploying certificate
Successfully deployed certificate for DOMAIN.NAME to /etc/nginx/nginx.conf
Congratulations! You have successfully enabled HTTPS on https://DOMAIN.NAME

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
If you like Certbot, please consider supporting our work by:
 * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
 * Donating to EFF:                    https://eff.org/donate-le
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Another restart of Nginx, just to be safe `systemctl restart nginx` and check with browser if everything works.

Feel free to check `/etc/nginx/nginx.conf` again to see how certbot did a good job of creating a `server` entry with TLS :)

