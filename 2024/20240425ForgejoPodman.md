# Running Forgejo with Podman

Goal: a simple setup to have an updateable, rootless forgejo container running as systemd service on a RHEL 9 (Red Hat Enterprise Linux) machine. Nginx as reverse proxy with a letsencrypt certificate. SSH on port 2022.

I expect that nginx, podman and container tools are already installed.

## Create data and config directories

I store all persistent files of my containers in `/var/pods`. This new container will be called forge, so let's create the directories for data and the `app.ini` file[^1]. The container will be run under the `builder` user, so let's make sure permissions and ownership are right.

```
# mkdir -p /var/pods/forge/config
# mkdir -p /var/pods/forge/data
# chown builder.builder /var/pods/forge/config
# chown builder.builder /var/pods/forge/data
# chmod 755 /var/pods/forge/config
# chmod 755 /var/pods/forge/data
```

## Pull container

We are using 7-rootless, so we can get updates through a simple Pull

```
# podman pull codeberg.org/forgejo/forgejo:7-rootless
```

## Create systemd service

I have this in my `/etc/systemd/system/forgejo.service` file:

```systemd
[Unit]
    Description=forgejo
    After=network-online.target
    Wants=network-online.target

[Service]
    ExecStartPre=mkdir -p /var/pods/forge/data
    ExecStartPre=mkdir -p /var/pods/forge/config
    ExecStartPre=-/bin/podman kill forge
    ExecStartPre=-/bin/podman rm forge
    ExecStart=/bin/podman run \
        --name forge \
        --volume /var/pods/forge/data:/var/lib/gitea:Z \
        --volume /var/pods/forge/config:/etc/gitea:Z \
        --shm-size 256m \
        --publish 4000:3000 \
        --publish 2022:2022 \
        codeberg.org/forgejo/forgejo:7-rootless
    ExecStop=/bin/podman stop forge

[Install]
    WantedBy=multi-user.target
```

Don't forget to `systemctl daemon-reload` after adding or changing this file :)

As you can see, I forward port 4000 to 3000 for the web part and 2022 goes to 2022 for the SSH server. As we are rootless, I cannot use port 22.

## Nginx config

This is the server entry in my `/etc/nginx/nginx.conf` file:

```nginx
server {
  server_name forge.example.com;
  location / {
    proxy_set_header   X-Real-IP $remote_addr;
    proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header   Host $host;
    proxy_pass         http://127.0.0.1:4000/;
    proxy_http_version 1.1;
    proxy_set_header   Upgrade $http_upgrade;
    proxy_set_header   Connection "upgrade";
  }
  listen [::]:443 ssl; # managed by Certbot
  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/forge.example.com/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/forge.example.com/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
  # set client body size to 100 MB #
  client_max_body_size 100M;
}
```

Just your normal reverse proxy config that forwards all requests to port 4000, so it ends up inside the container at port 3000.

## Firewall

For SSH connections to make it to the container, make sure to open port 2022 on your firewall.

## Start the container

A simple `systemctl start forgejo` should fire up the container and you should be able to see the installer page in your web browser. Configure your forgejo instance and check that stuff works as planned. If you have configured the basics, stop the container again with `systemctl stop forgejo` and check that you can see an `app.ini` file in `/var/pods/forge/config`.

You will have to correct the ini file by setting `SSH_LISTEN_PORT = 2022` to make sure you can connect via ssh for git operations.

## Update the container

When the forgejo team publishes a new release in the 7.x series, you can simply do

```
# systemctl stop forgejo
# podman pull codeberg.org/forgejo/forgejo:7-rootless
# systemctl start forgejo
```

and you are up2date again.

## Local config

On my laptop I have added this to `.ssh/config`:

```
Host forge.example.com
  Port 2022
```

So that I don't have to add port 2022 to every git command :)

[^1]: There is some work being done on removing the need for the config volume at https://codeberg.org/forgejo/forgejo/pulls/3363