# Tell Postfix to send emails via IPv4 or IPv6 only to specific recipient domains

**Not yet verified to work, will do that later today 2024-04-10**

To configure Postfix to use only IPv4 or IPv6 for specific outgoing connections to specific domains, we need to make the following changes:

1. Create rules in `/etc/postfix/master.cf` to limit external connections to either IPv4 or IPv6.

```
smtp-ipv4-only unix  -       -       n        -       -       smtp
       -o inet_protocols=ipv4
smtp-ipv6-only unix  -       -       n        -       -       smtp
       -o inet_protocols=ipv6
```

2. Tell postfix to use a transport map in `/etc/postfix/main.cf`

```
transport_maps = hash:/etc/postfix/transport
```

3. Create (or edit if already existing) the `/etc/postfix/transport` file and connect certain domains to the specific rules. 
```
examplev4only.com smtp-ipv4-only:
examplev6only.com smtp-ipv6-only:
```

4. Finally do a `postmap /etc/postfix/transport` and reload postfix to make sure its using the new configuration. This means for me to issue a `systemctl restart postfix`. You might also get away with a simple `postfix reload`command, but I prefer to have a clean restart of postfix after such config changes. Make sure to check `/var/log/maillog` to see if all is working and postfix is happy.

Verify that the rules are working as expected by sending emails to the specific domains while having an eye on `/var/log/maillog`.

Source: https://blog.schaal-24.de/mail/mails-mit-postfix-fuer-einzelne-domains-nur-ueber-ipv4-oderipv6-verschicken/