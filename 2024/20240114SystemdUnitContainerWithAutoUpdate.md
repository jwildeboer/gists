# Run a container as a service with Auto Update on restart

Neat little trick I learned from my colleague Andreas Stolzenberg today.

With this unit file in `/etc/systemd/system/forgejo.service` I have a forgejo container that will check for an update at every restart of the service. And if an update is available, it will pull the updates and start forgejo without interfering with my content.

It also gives me easy access to the `app.ini` file and logs.

`podman` can be so much fun!

Neat!

```systemd
[Unit]
    Description=forgejo
    After=network-online.target
    Wants=network-online.target

[Service]
    ExecStartPre=mkdir -p /var/pods/forgejo/config
    ExecStartPre=mkdir -p /var/pods/forgejo/data
    ExecStartPre=mkdir -p /var/pods/forgejo/log
    ExecStartPre=-/bin/podman kill forgejo
    ExecStartPre=-/bin/podman rm forgejo
    ExecStart=/bin/podman run \
        --name forgejo \
        --volume /var/pods/forgejo/config:/etc/gitea:Z \
        --volume /var/pods/forgejo/data:/var/lib/gitea:Z \
        --volume /var/pods/forgejo/log:/var/lib/gitea/data/log:Z \
        --shm-size 256m \
        --publish 4000:3000 \
        codeberg.org/forgejo/forgejo:1.21-rootless
    ExecStop=/bin/podman stop forgejo

[Install]
    WantedBy=multi-user.target
```

This works, because when I use `codeberg.org/forgejo/forgejo:1.21-rootless` I will automagically get the latest version (which currently is 1.21.3). It won't do updates to the next major version, as that will need manual intervention most probably.