# How I run web services as containers with Nginx and Letsencrypt on my RHEL9 server

* Create the container
* Create a basic server entry in nginx
* Run letsencrypt to switch to https and add the correct proxy settings
* Turn the container into a systemd service

## Create the container

I prefer to run simple containers, ideally ephemeral/stateless. So most often it's just `podman pull $IMAGE`. But in some cases the containers need dedicated storage or other things, so for this guide we will not really care about HOW we get the container up and running, we assume it is set up and configured the way it should and take it from there.

The important thing to note is the port on which the container offers its webservice. More precise: the port mapping on the host. Make a mental note of the port used on the host system that forwards to the port inside the container. We will need it later. For this guide we will assume that **port 8085** is the correct one.

With the container up **and running**, the port mapping can be tested for example with `curl localhost:8085` on the console of the host and we can now set up the nginx as reverse proxy.

## Create a basic server entry in nginx

I have a simple template for server entry that I just copy/paste, uncomment and edit in `/etc/nginx/nginx.conf` so that it has the correct domain name (which I obviously already have configured in DNS to point to this server)

```
# Add New Server and run certbot --nginx -d domain name
# server {
#     server_name example.com;
#     root /usr/share/nginx/html;
#     listen 80;
#     listen [::]:80;
# }
```

Let's use `example.wildeboer.net`

```nginx
# Add New Server and run certbot --nginx -d domain name
server {
    server_name example.wildeboer.net;
    root /usr/share/nginx/html;
    listen 80;
    listen [::]:80;
}
```

Restart nginx with `systemctl restart nginx` so that nginx starts serving an empty page for `http://example.wildeboer.net`. Check that everything is working correctly enough (you will get security warnings in your browser because this is for now just a simple `http` connection)

## Run letsencrypt to switch to https and add the correct proxy settings

Next we are going to set up letsencrypt, which will convert this simple entry to a correct `https` version with the certificates added.

`certbot --nginx -d example.wildeboer.net`

You'll get a lot of output of letsencrypt doing its thing, requesting and installing the certificates etc.

After all of that is done, you will find a new entry in `/etc/nginx/nginx.conf` that looks something like this:

```nginx
server {
  server_name  example.wildeboer.net;
  root         /usr/share/nginx/html;

  listen [::]:443 ssl ipv6only=on; # managed by Certbot
  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/example.wildeboer.net/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/example.wildeboer.net/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
```

Feel free to test it again. You should still get an empty page, but served via `https` :)

OK. Now we need to add the proxy config. I again have a little template for that, that mostly works. Some containers might need slightly different options, but this works for me in most cases.

```nginx
location / {
  proxy_set_header   X-Real-IP $remote_addr;
  proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header   Host $host;
  proxy_pass         http://127.0.0.1:8085/; # Correct local port here
  proxy_http_version 1.1;
  proxy_set_header   Upgrade $http_upgrade;
  proxy_set_header   Connection "upgrade";
}
```

This template replaces the `root /usr/share/nginx/html;` line. So our new complete entry should now looks like:

```nginx
server {
  server_name  example.wildeboer.net;
  location / {
    proxy_set_header   X-Real-IP $remote_addr;
    proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header   Host $host;
    proxy_pass         http://127.0.0.1:8085/; # Correct local port here
    proxy_http_version 1.1;
    proxy_set_header   Upgrade $http_upgrade;
    proxy_set_header   Connection "upgrade";
  }
  listen [::]:443 ssl ipv6only=on; # managed by Certbot
  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/example.wildeboer.net/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/example.wildeboer.net/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
```

Make sure to enter the local port (in our case 8085) to the `proxy_pass` line!

With the container still running, you should now be able to work with it when you open `https://example.wildeboer.net` in your browser!

## Turn the container into a systemd service

OK. Almost done. The final step is to do a `podman ps` and write down the NAME of the container. We will now turn this container into a systemd service:

```
> podman generate systemd --restart-policy=always -t 1 NAME > /etc/systemd/system/SERVICENAME.service
```

Have a look at this new service file and make changes, if needed. If all is good, stop the container  and enable and start the container as service.

```
> podman stop NAME
> systemctl daemon-reload
> systemctl enable SERVICENAME
> systemctl start SERVICENAME
```

A final check and done. Your container should now start after a reboot and nginx should happily serve it over the web. Letsencrypt will check in the background and automatically renew the certificates when needed.