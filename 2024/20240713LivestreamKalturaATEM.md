# Livestream with 2 cams to Kaltura

![Livestream setup in action](media/Livestream.jpeg "Livestream in action")

*Livestream setup in action*

## Goal

Send a livestream to Kaltura via RTMP(S) with primary and backup stream in 1080p, additional backup via webcam emulation to a hot-standby Google Meet on a laptop. Send prerecorded intro/outro. Use 2 cameras and speaker/audience microphones. Record all video and audio in good quality.

## Equipment used

- [ATEM Mini Extreme ISO](https://www.blackmagicdesign.com/products/atemmini/techspecs/W-APS-18) for all inputs and sending primary RTMP stream
- [ATEM Mini Pro](https://www.blackmagicdesign.com/de/products/atemmini/techspecs/W-APS-14) for sending backup RTMP stream
- [Hyperdeck Shuttle HD](https://www.blackmagicdesign.com/de/products/hyperdeckshuttlehd) for playing intro/outro clips on loop
- 2x [Blackmagic Pocket 4K cameras](https://www.blackmagicdesign.com/de/products/blackmagicpocketcinemacamera/techspecs/W-CIN-12)
- [SIRUI SH 15](https://store.sirui.com/de/products/sirui-sh15-aluminium-videostativ-mit-fluidkopf) Tripods for cameras
- 3x [SAMSUNG T5 1TB SSD](https://www.samsung.com/de/memory-storage/portable-ssd/portable-ssd-t5-1tb-black-mu-pa1t0b-eu/)
- [Hollyland MARS 300 Pro wireless HDMI](https://www.hollyland.com/product/mars-300-pro) for Camera 2
- [Tonor TW820](https://www.tonormic.com/products/tonor-tw-820-dual-wireless-microphone?variant=47253968716057) 2 Wireless microphones
- [Tonor TW822](https://www.tonormic.com/products/tonor-tw822-uhf-wireless-microphones-system-with-headset-lavalier-lapel-mics-bodypack-transmitter-receiver) 2 Wireless bodypacks with headset or lavalier
- 2x [TISINO XLR 2x female to 3.5mm](https://www.amazon.de/-/en/Tisino-Cable-Stereo-Connection-Microphone/dp/B07F3WC1FV) adapter cable  for microphones
- 2x [Neewer 660](https://de.neewer.com/products/neewer-cri-97-50w-660-prorgb-led-light-66600136) RGB LED lights
- 1x [GVM 80W](https://gvmled.com/product/gvm-80w-led-spotlight-daylight-kit-with-softboxbogo/) 5600K COB LED Studio light with diffusor
- Backdrop for presenter
- [TP-Link TL-SG108](https://www.tp-link.com/en/business-networking/soho-switch-unmanaged/tl-sg108/) 8 port 1GB Switch connected to public network
- Monitor screen 28" showing multiview from ATEM Extreme
- Laptop with ATEM software
- Laptop for Google Meet (additinal backup stream)
- [Headphones AKG K240](https://at.akg.com/pro/K240-Studio.html) for audio monitoring
- NP-F950 Batteries for Camera 2 and Hollyland

## Connections
![Connection Diagram](media/KalturaConnect.png "Connection Diagram of the setup")
### ATEM Mini Extreme ISO
![Mini Extreme](media/MiniExtreme.png "Back of ATEM Mini Extreme ISO with all ports")
- HEADPHONES to AKG K240
- MIC 1 to Tonor receiver for handheld microphones via XLR to 3.5 adapter
- MIC 2 to Tonor receiver for bodypacks via XLR to 3.5 adapter
- HDMI 1 to HDMI OUT on Hyperdeck Shuttle HD
- HDMI 2 to laptop for slides
- HDMI 3 to HDMI OUT on Camera 1
- HDMI 4 to HDMI OUT on Hollyland MARS 300 receiver, connected to Camera 2
- HDMI 5-8 not connected
- HDMI OUT 1 to HDMI1 on ATEM Mini Pro, sending PGM (Program)
- HDMI OUT 2 to Monitor, sending MULTIVIEW
- USB A to SAMSUNG T5 for ISO recording
- USB B to control laptop 
- NETWORK to network switch, sending primary RTMP stream to Kaltura

### ATEM Mini Pro
![Mini Pro](media/MiniPro.png "Back of ATEM Mini Pro with all ports")
- MIC 1-2 not connected
- HDMI 1 to HDMI OUT2 on ATEM Mini Extreme ISO, receives Program feed
- HDMI 2-4 not connected
- HDMI OUT to room system, sends Program (PGM)
- USB OUT to second laptop for secondary backup stream to Google Meet via Wireless
- NETWORK to network switch, sending backup RTMP stream to Kaltura

### Hyperdeck Shuttle HD
![Shuttle HD](media/ShuttleHD.png "Back of Hyperdeck Shuttle HD with all ports")
- HDMI IN not connected
- HDMI OUT to HDMI1 on ATEM Mini Extreme ISO, sending intro/outro clips on loop
- EXT DISK not connected
- SD CARD with intro/outro mp4 clips
- NETWORK to network switch

## Room Setup

- 1 Neewer 660 with red as background effect light on left side of backdrop
- 1 Neewer 660 with blue as background effect light on right side of backdrop
- Camera 1 in front of backdrop, picture cropped to backdrop
- GVM LED 1m left to camera 1 for speaker spotlight
- Camera 2 audience cam positioned on far right side of room
- Director table with all equipment

## Camera config

- SAMSUNG T5 connected to USB
- Record in 4K/Q3 Blackmagic Raw, LUT embedded
- White balance set to 5600K
- ISO to 400 (depending on light in room, 400 works with sunny sky)
- 180° shutter
- Lenses between f2.9 and f6.8
- Film to extended video LUT v5

## ATEM config

- Output is 1080p30
- Limiter on Master audio
- Split MIC1 to two audio channels
- Equaliser with band 1 (high-pass) and 5 (low-pass) activated on MIC1 L+R
- Expander on MIC1 L+R
- Compressor optional on MIC1 L+R
- Kaltura RTMP URL, **set Key to 1**, quality: streaming high is sufficient
- Add streaming status to multiview
