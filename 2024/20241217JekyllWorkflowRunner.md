# Using Codeberg/Forgejo actions to build and publish a Jekyll website

This workflow file for a [Forgejo runner](https://forgejo.org/docs/latest/admin/runner-installation/$0) takes the Jekyll source from the repository it is called from and generates the static files it then puts in another repo called `web`.

Uses the `ghcr.io/some-natalie/jekyll-in-a-can:latest` secure build container from [Natalie](https://some-natalie.dev/blog/jekyll-in-a-can/$0)

In the workflow file I set the `SITE` environment variable that defines the directory in the `web` repo to store the results. I have also defined two secrets in Forgejo:

- `PUSHER` is a token that allows read/write access to repositories of the user
- `CBMAIL` is the e-mail address to use for the git config

All the other needed parts are constructed from the default environment variables available through some clever regexes and `sed`.

To run your own runner on your Linux machine and make it known to [codeberg](https://codeberg.org) and receive jobs from your repos there, see my [blog entry "Running a runner"](https://jan.wildeboer.net/2024/08/Running-a-runner-codeberg/$0) where I explain how to do that.

```yaml
on: [push]
jobs:
  build:
    runs-on: podman
    container:
      image: ghcr.io/some-natalie/jekyll-in-a-can:latest
    env:
      SITE: "sandbox.wildeboer.net"
    steps:
      - name: Checkout sources and target
        run: |
          git config --global --add safe.directory /workspace/$GITHUB_REPOSITORY
          git config --global user.email "${{ secrets.CBMAIL }}"
          git config --global user.name "Forgejo Site Builder"
          git clone --depth 1 $(echo ${{ github.server_url }} | sed -e "s|://|://${{ github.token }}@|")/$GITHUB_REPOSITORY.git
          git clone $(echo ${{ github.server_url }} | sed -e "s|://|://${{ secrets.PUSHER }}@|")/$GITHUB_REPOSITORY_OWNER/web.git
      - name: Fetch and install needed GEMs
        run: |
          cd $(echo $GITHUB_REPOSITORY | sed 's|.*\/\(.*\)|\1|')
          bundle install          
      - name: Run Jekyll build 
        run: |
          cd $(echo $GITHUB_REPOSITORY | sed 's|.*\/\(.*\)|\1|')
          bundle exec jekyll build --destination ../web/$SITE          
      - name: Publish site
        run: |
          cd web
          git add $SITE
          git commit -m "Jekyll build of $GITHUB_REPOSITORY for https://$SITE done at $( env TZ=Europe/Berlin date +"%Y-%m-%d %X %z %Z" )"
          git push
```

Let's go through the file step by step and explain exactly what is happening.

```yaml
on: [push]
jobs:
  build:
    runs-on: podman
    container:
      image: ghcr.io/some-natalie/jekyll-in-a-can:latest
```

More or less standard boilerplate. The action gets triggered by a `push` to the repository, finds a runner that offers the `podman` label and tells it to get the `ghcr.io/some-natalie/jekyll-in-a-can:latest` container image to run the job.

```yaml
    env:
      SITE: "sandbox.wildeboer.net"
```

Now we set an environment variable that we use throughout the workflow as the target directory for the generated files. It is also the name of the site we generate the content for.

```yaml
   steps:
      - name: Checkout sources and target
        run: |
          git config --global --add safe.directory /workspace/$GITHUB_REPOSITORY
          git config --global user.email "${{ secrets.CBMAIL }}"
          git config --global user.name "Forgejo Site Builder"
          git clone --depth 1 $(echo ${{ github.server_url }} | sed -e "s|://|://${{ github.token }}@|")/$GITHUB_REPOSITORY.git
          git clone $(echo ${{ github.server_url }} | sed -e "s|://|://${{ secrets.PUSHER }}@|")/$GITHUB_REPOSITORY_OWNER/web.git
``` 

Now, we don't use the github style `checkout@V4` action, as that needs a working `nodejs` environment that our image doesn't offer. Not a big problem though, we just set some standard `git` config stuff and fetch the two repos we need. Workflows get an auth token with every job that allows read/write access to the repo that called the workflow, so we clone that repo next. We use `depth 1` as we don't need the full history, just the current version of the files.

The way I run my static websites is that I collect all the files needed in a git repo called `web`, which we clone next. Unfortunately our token does not allow access to that repo, so I defined a secret in Forgejo called `PUSHER`, which is another auth token that gives us read/write access to all repos I own.

What those confusing `sed` commands do is quite simple, actually. We get the URL to our server as environment variable `${{ github.server_url }}` but for the cloen operation to work, we need to change it to contain the auth token and an @ symbol in front of the hostname. That's exactly what the `sed` command does for us. It turns `https://my.forge.example` into `https://<TOKEN>@my.forge.example` so that we get read/write access to the repo.

Nice. We have two repos to work with. One with the Jekyll source and one where we can put the output. Let's go!

```yaml
      - name: Fetch and install needed GEMs
        run: |
          cd $(echo $GITHUB_REPOSITORY | sed 's|.*\/\(.*\)|\1|')
          bundle install
``` 

We find the name of the repository in `$GITHUB_REPOSITORY` but that unfortunately lloks like this: `username/repository`. The cloned repo however is in the directory called just `reponame`. `sed` to the rescue again! With the regex `'s|.*\/\(.*\)|\1|'` we split the input at the `/` characzer and capture the characters that come after that `/`. So now we have just `reponame` and can `cd` into that directory.

Before we can build our site, we have to fetch all needed gems and other packages, which the `bundle install` command does for us.

```yaml
      - name: Run Jekyll build 
        run: |
          cd $(echo $GITHUB_REPOSITORY | sed 's|.*\/\(.*\)|\1|')
          bundle exec jekyll build --destination ../web/$SITE          
``` 

Now we are ready to build our site! Note however that with every new step in the workflow we start at the main directory, so we need to do the `sed` and `cd` dance again before we can start the `jekyll build`. We also set the output directory to `../web/$SITE` (remember `$SITE` from the top?) to have the generated files right where we want them to be.

Almost done!

```yaml
      - name: Publish site
        run: |
          cd web
          git add $SITE
          git commit -m "Jekyll build of $GITHUB_REPOSITORY for https://$SITE done at $( env TZ=Europe/Berlin date +"%Y-%m-%d %X %z %Z" )"
          git push
```

So we now have the output in the `web` directory, which is the git repo where we want to store our static sites. We `cd` in to the `web` directory, add the generated files to the repo, commit the changes and push them to the remote. Done!

Our workflow has (hopefully) finished succesfully. The forgejo runner will now clean up everything and we are ready for the next run after the next change to our Jekyll site.