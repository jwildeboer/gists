# Gists

A collection of short notes on specific little things that are good to store and share but not (yet) enough content for a blog entry. Mostly geeky stuff.

I braindump stuff in a date stamped markdown file here and might turn them later on into wiki pages or blog entries. We shall see :)

And you are absolutely free to use the content in any way without any obligations. It's all under the [CC0](LICENSE) license.

## 2025

* [2025-03-02 A Poor Man's Container Registry/Cache](2025/20250302ForgejoContainerReg.md)
* [2025-03-01 Backup Forgejo](2025/20250301ForgejoBackup.md)
* [2025-02-21 Hardware used for my homelab](2025/20250221HomelabInv.md)

## 2024

* [2024-12-17 Using Codeberg/Forgejo actions to build and publish a Jekyll website](2024/20241217JekyllWorkflowRunner.md)
* [2024-08-04 Running the forgejo-runner as user systemd service on RHEL9](2024/20240804ForgejoRunnerAsUserService.md)
* [2024-07-13 Livestream with 2 cams to Kaltura](2024/20240713LivestreamKalturaATEM.md)
* [2024-07-05 Add server entry, certificate etc to nginx](2024/20240705AddServerToNginx.md)
* [2024-06-23 How I run web services as containers with Nginx and Letsencrypt on my RHEL9 server](2024/20240623NginxContainers.md)
* [2024-06-23 Updating masto-thread-renderer](2024/20240623MastoThreadRenderer.md)
* [2024-04-28 Find attachments when parsing emails in a robust way](2024/20240428EmailParsingAttachments.md)
* [2024-04-25 Running Forgejo with Podman](2024/20240425ForgejoPodman.md)
* [2024-04-21 How to configure Forgejo to send and receive e-mail](2024/20240421ForgejoMail.md)
* [2024-04-10 Tell Postfix to send emails via IPv4 or IPv6 only to specific recipient domains](2024/20240410PostfixIPv4IPv6.md)
* [2024-04-01 Mirror of FAQ on the xz-utils backdoor (CVE-2024-3094) by thesamesam](2024/20240401CVE20243094FAQMirror.md)
* [2024-01-14 Run a container as a service with Auto Update on restart](2024/20240114SystemdUnitContainerWithAutoUpdate.md)

## 2023

* [2023-05-29 SVG of the dutch krul symbol](2023/20230529krul.svg/krul.svg)
* [2023-05-06 Work with RKI Data for coronamuc](2023/20230605RKIStuff.md)
* [2023-04-22 Regexes and shell scripts to find abusive IPs attacking my mail server](2023/20230422LogCheck.md)
* [2023-04-11 Hex dump of Deutschlandticket paper ticket](2023/20230411DTicketHex)
* [2023-03-05 A very simple mailing list with sieve](2023/20230305SieveML.md)

## 2022

* [2022-11-07 Convert GIF to webm or MP4 with ffmpeg](2022/20221107GifToVid.md)
* [2022-07-18 Analysing a DMARC report](2022/20220718DMARC.md)
* [2022-11-01 Health check: Distribution of followers on Mastodon](2022/20221101MastoFollowList.md)
* [2022-08-15 Block a whole CIDR away forever](2022/20220815Firewall.md)
* [2022-08-12 Find brute-force SASL logins](2022/20220812SASL-Brute-Force.md)
* [2022-07-22 Update Codeberg Pages with Jekyll and Woodpecker as CI](2022/20220722WoodpeckerJekyll.md)
* [2022-07-18 A complete incoming mail transaction](2022/20220718maillog.md)
* [2022-07-18 Analysing a DMARC report](2022/20220718DMARC.md)
