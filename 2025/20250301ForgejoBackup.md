# Backup Forgejo

I run my forgejo instance as a systemd service which starts/stops the container, with two directories from the host system mounted: `/var/pods/forge/data` for the forgejo data directory and `/var/pods/forge/config` for the `app.ini` file.

Backups go to `/var/backup` on the host and are named `<iso-date>-forgejo.tgz`.

To perform the backup, we need to stop the forgejo container, create the backup and start forgejo again. This is handled with a very simple shell script called `/usr/bin/local/fjbackup`:

```sh
#!/bin/sh
cd /var/backup
echo -n "Stopping forgejo ..."
systemctl stop forgejo
echo " done"
echo -n "Starting backup ..."
tar cfz `/usr/bin/date -I`-forgejo.tgz /var/pods/forge > /dev/null 2>&1
echo " done"
echo -n "Starting forgejo ..."
systemctl start forgejo
echo " done"
echo "Created /var/backup/"`/usr/bin/date -I`"-forgejo.tgz succesfully."
```

In future I will make this script a bit more helpful, by keeping only 3 backups or something. But for now it does the job. I typically run it once a week manually and before I do an update/upgrade of forgejo.