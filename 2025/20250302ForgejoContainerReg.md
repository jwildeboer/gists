# A Poor Man's Container Registry/Cache

One of the advantages of using [forgejo](https://forgejo.org) is that it can also be used as an OCI-compliant [container registry](https://forgejo.org/docs/latest/user/packages/container/$0), meaning you can keep the containers you need readily available and you don't have to download images all the time from docker, ghcr.io etc., where you might run into rate limits.

How to? Quite simple:

- [Create auth token](https://docs.codeberg.org/advanced/access-token/$0) with read/write permission for packages in Forgejo
- Use that token as password when logging in from your build machine with podman
- Use podman's `--authfile` option to make login persistent across reboots

```sh
podman login --authfile ~/.config/containers/auth.json forge.wildeboer.net
```

Grab a container image you want to host on your own little registry. Or build one locally. Just make sure your local `podman` has it.

```sh
podman pull ghcr.io/some-natalie/jekyll-in-a-can:latest
``` 

Now you can push that image to Forgejo (in my example I put it in the `webhosting` organisation I have created on my forgejo instance)

```sh
podman push ghcr.io/some-natalie/jekyll-in-a-can forge.wildeboer.net/webhosting/jekyll-in-a-can:latest
```

And if you need that container on one of your machines, you just

```sh
podman pull forge.wildeboer.net/webhosting/jekyll-in-a-can:latest
```

## Automating container updates

Every wednesday evening, a cronjob runs that pulls the latest version of the jekyll-in-a-can container image, extracts the creation date as a tag and pushes the container image to my forgejo instance. I might extend this crude script by looping through a list of containers I'd like to keep around, but for now this is GoodEnough.

```sh
#!/bin/sh
echo "Pulling jekyll-in-a-can from ghcr.io ..."
podman pull ghcr.io/some-natalie/jekyll-in-a-can:latest
TAGDATE=`/usr/bin/podman inspect --format "{{.Created}}" ghcr.io/some-natalie/jekyll-in-a-can | grep -o "[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}"`
echo "Received the container image with creation date $TAGDATE"
echo "done"
echo "Authenticating with forge.wildeboer.net ..."
podman login --authfile /etc/sysconfig/crauth.json forge.wildeboer.net
echo "done"
echo "Pushing the :latest tag to forge.wildeboer.net ..."
podman push ghcr.io/some-natalie/jekyll-in-a-can:latest forge.wildeboer.net/webhosting/jekyll-in-a-can:latest
echo "done"
echo "Pushing the $TAGDATE tag to forge.wildeboer.net ..."
podman push ghcr.io/some-natalie/jekyll-in-a-can:latest forge.wildeboer.net/webhosting/jekyll-in-a-can:$TAGDATE
echo "done. Job finished. CU next week."
```