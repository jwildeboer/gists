# Hardware used for my homelab

![Homelab01.jpg](Homelab01.jpg)

## Rack

| Description | Price | Art.-No / Link |
| :--- | ---: | :--- |
| 1x DIGITUS 254 mm (10") 6U wall mounting cabinet | €60,00 | [DN-10-05U-1](https://de.assmann.shop/en/IT-Infrastructure/Network-Cabinets-Wall-Mounting/10-Wall-Mounting-Cabinets/Wall-mounted-housing-254-mm-10-312x300-mm-WxD.html) |
| 1x DIGITUS 254 mm (10") 1U shelf | €11,00 | [DN-10 TRAY-1](https://de.assmann.shop/en/IT-Infrastructure/Accessories/Shelves/254-mm-10-1U-shelf-var.html) |
| 2x DIGITUS Modular Patch Panel, 12-port | €11,00/pc | [DN-91420](https://de.assmann.shop/en/Copper-Network-Technology/Modular-Connection-Technology/Patch-Panels-Keystone/Modular-Patch-Panel-12-port-var.html) |
| 5x 3D printed Keystone covers | €0,05/pc | [3D Model](https://www.printables.com/model/1192720-keystone-blank$0) |
| 2x 3D printed mount for ThinkCentre Tiny M9xx | €2,00 | [3D Model](https://www.printables.com/model/1175268-lenovo-thinkcentre-m-10-rack-mount) |
| 1x 3D printed brackets for Network Switch | €1,00 | [3D Model](https://www.printables.com/model/1050578-switch-mounting-bracket-for-eiflex-poe-switch-x8-1) |
| **TOTAL (ca.)** | €96,00||

## Devices

| Description | Price | Art.-No / Link |
| :--- | ---: | :--- |
| 1x VIMIN 10-Port Gigabit PoE Switch with 8 Port PoE | €50,00 | [VM-0820GP](https://vimintech.com/products/upgraded-10-port-gigabit-poe-switch-with-8-port-poe-vimin-8-port-unmanaged-10-100-1000mbps-network-poe-switch-with-2-uplink-ports-ai-watchdog-vlan-extend-250m-support-ieee802-3af-at) |
| 1x Thinkpad ThinkCentre M910x Tiny (used, eBay) | €100 | Type M910 Tiny 10MY |
| 1x Thinkpad ThinkCentre M900 Tiny (used, eBay) | €80 | Type M900 Tiny 10FL |
| **TOTAL (ca.)** | €230,00||

## Cables & accessories

| Description | Price | Art.-No / Link |
| :--- | ---: | :--- |
| 4x UGREEN USB-A to USB 3.0 Kabel Super Speed | €5,00/pc | [UGREEN 10369](https://de.ugreen.com/collections/kabel/products/ugreen-usb-kabel-3-0-super-speed-kabel-a-stecker-auf-a-stecker-usb-verbindungskabel-kompatibel-mit-drucker-modems-festplatten-kameras-usw-0-5m?variant=44566287778104) |
| 2x AISENS DISPLAYPORT to HDMI Converter Cable, DP/M-HDMI/M, Black, 0.5M | €10,00/pc | [AISENS A125-0550](https://aisenstech.com/producto/aisens-cable-conversor-displayport-a-hdmi-dp-m-hdmi-m-negro-0-5m/) |
| 12x kwmobile Keystone Module for CAT 6A Cable - 10 Gbit/s Shielded Metal | €2,00/pc | [kwmobile 52348.01.06_m001985](https://www.amazon.de/-/en/kwmobile-Keystone-Module-CAT-Cable/dp/B08BHKC31G) |
| 10x deleyCON 0.25m CAT6 Patch Cable | €1,20/pc | [MK2246](https://deleycon.com/deleycon-0-25m-9-84-inch-cat6-patch-cable-s-ftp-pimf-shielding-cat-6-rj45-lan-dsl-network-ethernet-switch-router-modem-access-point-patch-fields-black/) |
| 4x AAOTOKK USB 3.0 A Keystone Adapter Female to Female USB 3.0 A | €2,11/pc | [682559179910](https://www.amazon.de/dp/B07Z9446ZR) |
| 2x AAOTOKK HDMI Keystone Module Female to Female | €2,15/pc | [682559179811](https://www.amazon.de/dp/B07Z8YSZH7) |
| **TOTAL (ca.)** | €90,00||

## Other parts I already had

| Description |
| :--- |
| 1TB NVME m2 SSD |
| 256GB SATA SSD |
| 2x 16GB SoDIMM |
| 2x 50cm network patch cable |
| Screws and blocks for rack mount |

## A bit more details on the two PCs

At the bottom is a ThinkCentre M900 Tiny (10FLS4W900) with 4x Intel(R) Core(TM) i5-6500T CPU @ 2.50GHz, 1x 8GB DDR4-2133, 256GB SAMSUNG MZ7TY256HDHP-000L7 which will soon be updated with more RAM and a NVME m2 SSD.

Above that one is a ThinkCentre M910x Tiny (10MYS03U00) with 4x Intel(R) Core(TM) i5-7500 CPU @ 3.40GHz, 1x 16GB DDR-2133, 1TB KINGSTON SNV2S1000G NVME m2 SSD.

Both machines have Red Hat Enterprise Linux release 9.5 (Plow) installed. Both have podman and container tools installed and both have the forgejo-runner up and running as systemd service.
